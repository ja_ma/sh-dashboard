mqtt:
  # The MQTT broker to connect to
  server: tcp://192.168.0.103:1883
  # Optional: Username and Password for authenticating with the MQTT Server
  user: mqtt2prometheus
  password: mqtt2prometheus123
  # Optional: for TLS client certificates
  # ca_cert: certs/AmazonRootCA1.pem
  # client_cert: certs/xxxxx-certificate.pem.crt
  # client_key: certs/xxxxx-private.pem.key
  # Optional: Used to specify ClientID. The default is <hostname>-<pid>
  # client_id: somedevice
  # The Topic path to subscribe to. Be aware that you have to specify the wildcard, if you want to follow topics for multiple sensors.
  topic_path: solarTracer/values/#
  # Optional: Regular expression to extract the device ID from the topic path. The default regular expression, assumes
  # that the last "element" of the topic_path is the device id.
  # The regular expression must contain a named capture group with the name deviceid
  # For example the expression for tasamota based sensors is "tele/(?P<deviceid>.*)/.*"
  # device_id_regex: "(.*/)?(?P<deviceid>.*)"
  # The MQTT QoS level
  qos: 0
  # NOTE: Only one of metric_per_topic_config or object_per_topic_config should be specified in the configuration
  # Optional: Configures mqtt2prometheus to expect a single metric to be published as the value on an mqtt topic.
  metric_per_topic_config:
    # A regex used for extracting the metric name from the topic. Must contain a named group for `metricname`.
    metric_name_regex: "(.*/)?(?P<metricname>.*)"
  # Optional: Configures mqtt2prometheus to expect an object containing multiple metrics to be published as the value on an mqtt topic.
  # This is the default.
  # object_per_topic_config:
    # The encoding of the object, currently only json is supported
    # encoding: json
cache:
  # Timeout. Each received metric will be presented for this time if no update is send via MQTT.
  # Set the timeout to -1 to disable the deletion of metrics from the cache. The exporter presents the ingest timestamp
  # to prometheus.
  timeout: 24h
json_parsing:
  # Separator. Used to split path to elements when accessing json fields.
  # You can access json fields with dots in it. F.E. {"key.name": {"nested": "value"}}
  # Just set separator to -> and use key.name->nested as mqtt_name
  separator: .
# This is a list of valid metrics. Only metrics listed here will be exported
metrics:
  # The name of the metric in prometheus
  - prom_name: test
    # The name of the metric in a MQTT JSON message
    mqtt_name: test
    # The prometheus help text for this metric
    help: test
    # The prometheus type for this metric. Valid values are: "gauge" and "counter"
    type: gauge
    # A map of string to string for constant labels. This labels will be attached to every prometheus metric
    const_labels:
      label: general
      sensor_type: solar

  # General
  - prom_name: solar_internal_status
    mqtt_name: internal_status
    help: Internal status of communication with regulator
    type: gauge
    const_labels:
      sensor_type: solar
      label: general

  - prom_name: solar_controller_temperature
    mqtt_name: controller_temperature
    help: Charge controller temperature in Celsius
    type: gauge
    const_labels:
      sensor_type: solar
      label: general

  - prom_name: solar_controller_heatsink_temperature
    mqtt_name: controller_heatsink_temperature
    help: Charge controller heatsink temperature in Celsius
    type: gauge
    const_labels:
      sensor_type: solar
      label: general

  # Battery
  - prom_name: solar_battery_soc
    mqtt_name: battery_soc
    help: Battery state of charge in percent
    type: gauge
    const_labels:
      sensor_type: solar
      label: battery

  - prom_name: solar_battery_charging_power
    mqtt_name: battery_charging_power
    help: Battery charging power in Watts
    type: gauge
    const_labels:
      sensor_type: solar
      label: battery

  - prom_name: solar_battery_charging_current
    mqtt_name: battery_charging_current
    help: Battery charging current in Amps
    type: gauge
    const_labels:
      sensor_type: solar
      label: battery

  - prom_name: solar_battery_voltage
    mqtt_name: battery_voltage
    help: Battery voltage in Volts
    type: gauge
    const_labels:
      sensor_type: solar
      label: battery

  - prom_name: solar_battery_overall_current
    mqtt_name: battery_overall_current
    help: Battery overall current in Amps
    type: gauge
    const_labels:
      sensor_type: solar
      label: battery

  # PV
  - prom_name: solar_pv_voltage
    mqtt_name: pv_voltage
    help: Photovoltaic panel voltage in Volts
    type: gauge
    const_labels:
      sensor_type: solar
      label: pv

  - prom_name: solar_pv_power
    mqtt_name: pv_power
    help: Photovoltaic panel power in Watts
    type: gauge
    const_labels:
      sensor_type: solar
      label: pv

  - prom_name: solar_pv_current
    mqtt_name: pv_current
    help: Photovoltaic panel current in Amps
    type: gauge
    const_labels:
      sensor_type: solar
      label: pv

  # LOAD
  - prom_name: solar_load_power
    mqtt_name: load_power
    help: Load power in Watts
    type: gauge
    const_labels:
      sensor_type: solar
      label: pv

  - prom_name: solar_load_current
    mqtt_name: load_current
    help: Load current in Amps
    type: gauge
    const_labels:
      sensor_type: solar
      label: pv