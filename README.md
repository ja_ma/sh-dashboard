# Smart Home Dashboard #

This repository runs Grafana backed with Prometheus, Telegraf and InfluxDB, everything wrapped in Docker containers.

### Grafana dashboards

- Resource monitor - resource monitor
- Weather - module to watch real time and historical weather data
- Water level - module for water level measurement using HC-SR05 sensor
- Indoor air quality - module to monitor IAQ using DHT11 sensor

### How do I get set up? ###

Resource monitor:
1. Run sh-rm project according to its readme.

Weather:
- Running automatically

Water level:
1. Configure IP of WLM's Prometheus client in `./prometheus/config/prometheus.yml` jobs to IP of machine prometheus client is running on. E.g. for master job it can be is: `192.168.0.103:9091`.
2. Run sh-wlm project according to its readme.

Indoor air quality:
1. Configure IP of IAQ's Prometheus client in `./prometheus/config/prometheus.yml` jobs to IP of machine prometheus client is running on. E.g. for master job it can be is: `192.168.0.103:9092`.
2. Run sh-iaq project according to its readme.

Run the docker-compose file and connect to: http://localhost:3000/.

### How to run the solution ###
Via command: `docker-compose up`. Eg for linux on pi this may be helpful:
- `nohup docker-compose -f /home/mmajdis/Projects/sh/sh-dashboard/docker-compose.yml up>>/home/mmajdis/Projects/sh/sh-dashboard/output.log &`
- (optional) - add the command to `/etc/rc.local` to run it on startup 
- (optional) - add the file in  `./logrotate` to `/etc/logrotate.d` folder on your machine to rotate logs